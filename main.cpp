#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>
#include <unistd.h>
#include <fcntl.h>

#include <libdrm/amdgpu.h>
#include <libdrm/amdgpu_drm.h>
#include <xf86drm.h>
#include <cassert>
#include <cmath>
#include <cstring>
#include <fstream>
#include <array>
#include <set>

struct Buffer {
    std::uint64_t va;
    std::uint64_t size;
    amdgpu_bo_handle handle;
    amdgpu_va_handle va_handle;
    void *ptr;
};

enum Memory_type {
  VRAM = AMDGPU_GEM_DOMAIN_VRAM,
  GTT = AMDGPU_GEM_DOMAIN_GTT,
  DOORBELL = AMDGPU_GEM_DOMAIN_DOORBELL
};

class Amd_device {
public:
    explicit Amd_device(const char *filename);

    ~Amd_device();

    Buffer allocate(std::uint64_t size, Memory_type mem_type);

    std::uint32_t create_queue(Buffer& doorbell, Buffer& ring, Buffer& rptr, Buffer& wptr, Buffer& shadow);

    void destroy_queue(unsigned queue_id);
private:
    amdgpu_device_handle dev_;
    amdgpu_context_handle ctx_;
};

Amd_device::Amd_device(const char *filename) {
    int fd = open(filename, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "Could not open device file\n");
        throw -1;
    }

    std::uint32_t maj, min;
    int r = amdgpu_device_initialize(fd, &maj, &min, &dev_);
    close(fd);
    if (r) {
        fprintf(stderr, "Couldn't initialize libdrm_amdgpu from device fd\n");
        throw -1;
    }

    r = amdgpu_cs_ctx_create(dev_, &ctx_);
    if (r) {
        amdgpu_device_deinitialize(dev_);
        fprintf(stderr, "Could not create a context\n");
        throw -1;
    }
}

Amd_device::~Amd_device() {
    amdgpu_device_deinitialize(dev_);
}

Buffer Amd_device::allocate(std::uint64_t size, Memory_type mem_type) {
    Buffer buf = {};
    buf.size = size;
    
    std::size_t alignment = 2 * 1024 * 1024;
    if (mem_type == DOORBELL || size <= 4096)
      alignment = 4096;

    size = (size + alignment - 1) & ~(alignment - 1);
    struct amdgpu_bo_alloc_request alloc_req = {
            .alloc_size = size,
            .phys_alignment = alignment,
            .preferred_heap = (std::uint32_t)mem_type,
            .flags = ((mem_type == DOORBELL) ?  0 : AMDGPU_GEM_CREATE_VM_ALWAYS_VALID) | AMDGPU_GEM_CREATE_CPU_GTT_USWC
    };
    int r = amdgpu_bo_alloc(dev_, &alloc_req, &buf.handle);
    if (r) {
        fprintf(stderr, "Failed to allocate buffer %d %s\n", r, strerror(errno));
        throw -1;
    }

    r = amdgpu_bo_cpu_map(buf.handle, &buf.ptr);
    if (r) {
        fprintf(stderr, "Failed to map buffer into CPU address space\n");
        amdgpu_bo_free(buf.handle);
        throw -1;
    }
    
    if (mem_type == DOORBELL)
      return buf;

    r = amdgpu_va_range_alloc(dev_, amdgpu_gpu_va_range_general, size, alignment, 0, &buf.va, &buf.va_handle, 0);
    if (r) {
        fprintf(stderr, "Failed to allocate VA space for buffer\n");
        amdgpu_bo_free(buf.handle);
        throw -1;
    }

    r = amdgpu_bo_va_op(buf.handle, 0, size, buf.va, 0, AMDGPU_VA_OP_MAP);
    if (r) {
        fprintf(stderr, "Failed to map VA space for buffer\n");
        amdgpu_bo_free(buf.handle);
        amdgpu_va_range_free(buf.va_handle);
        throw -1;
    }
    return buf;
}

std::uint32_t Amd_device::create_queue(Buffer& doorbell, Buffer& ring, Buffer& rptr, Buffer& wptr, Buffer& shadow) {
    std::uint32_t doorbell_handle;
    int r;
    
    r = amdgpu_bo_export(doorbell.handle, amdgpu_bo_handle_type_kms, &doorbell_handle);
    if (r) {
        fprintf(stderr, "Failed to get kms handle for doorbell BO\n");
        throw -1;
    }
    
    union drm_amdgpu_userq args;
    memset(&args, 0, sizeof(args));
    args.in.op = AMDGPU_USERQ_OP_CREATE;
    args.in.mqd.ip_type = AMDGPU_HW_IP_GFX;
    args.in.mqd.doorbell_handle = doorbell_handle;
    args.in.mqd.queue_va = ring.va;
    args.in.mqd.queue_size = ring.size;
    args.in.mqd.rptr_va = rptr.va;
    args.in.mqd.wptr_va = wptr.va;
    args.in.mqd.shadow_va = shadow.va;
    
    r = drmCommandWriteRead(amdgpu_device_get_fd(dev_), DRM_AMDGPU_USERQ, &args, sizeof(args));
    
    if (r != 0) {
        fprintf(stderr, "user queue creation failed: %d %s\n", errno, strerror(errno));
        throw -1;
    }
    return args.out.queue_id;
}

void Amd_device::destroy_queue(unsigned queue_id) {
    union drm_amdgpu_userq args;
    memset(&args, 0, sizeof(args));
    args.in.op = AMDGPU_USERQ_OP_FREE;
    args.in.queue_id = queue_id;

    int r = drmCommandWriteRead(amdgpu_device_get_fd(dev_), DRM_AMDGPU_USERQ, &args, sizeof(args));

    if (r != 0) {
        fprintf(stderr, "user queue creation failed: %d %s\n", errno, strerror(errno));
        throw -1;
    }
}

void pkt3(std::uint32_t *&ptr, unsigned op, unsigned count, unsigned predicate = 0, unsigned shader_type = 0) {
    assert(count >= 1 && count < 0x4002);
    assert(op < 0x100);
    count = (count - 2) & 0x3fff;
    *ptr++ = (0x3u << 30) | (count << 16) | (op << 8) | predicate | (shader_type << 1);
}

void nop(std::uint32_t *&ptr, unsigned cnt = 1) {
    pkt3(ptr, 0x10, cnt);
    ptr += cnt - 1;
}

double gettime() {
    struct timespec ts;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
    return ts.tv_sec + ts.tv_nsec/1e9;
}

#define PACKET3_WRITE_DATA		0x37
#define WR_CONFIRM			(1 << 20)
#define WRITE_DATA_DST_SEL(x)		((x) << 8)
#define WRITE_DATA_ENGINE_SEL(x)	((x) << 30)
#define  WRITE_DATA_CACHE_POLICY(x)	((x) << 25)


int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "cmdline: usermode-queue dri-render-node\n");
        return 1;
    }
    Amd_device dev(argv[1]);
    
    Buffer doorbell = dev.allocate(4096, DOORBELL);
    Buffer ring = dev.allocate(2 * 1024 * 1024, GTT);
    Buffer rptr = dev.allocate(4096, GTT);
    Buffer wptr = dev.allocate(4096, GTT);
    Buffer shadow = dev.allocate(16384, GTT);
    Buffer destbuf = dev.allocate(4096, GTT);
    
    auto queue = dev.create_queue(doorbell, ring, rptr, wptr, shadow);
    
    sleep(1);
    
    std::uint32_t *cmdbuf = (std::uint32_t*)ring.ptr;
    auto cmdbuf_base = cmdbuf;
    
    for (unsigned i = 0; i < 512 * 1024; ++i)
        pkt3(cmdbuf, 0x10, 1);
    cmdbuf = cmdbuf_base;

    for (unsigned i = 0; i < 1; ++i) {
        pkt3(cmdbuf, PACKET3_WRITE_DATA, 9);
        *cmdbuf++ = WRITE_DATA_DST_SEL(5) | WR_CONFIRM | WRITE_DATA_CACHE_POLICY(3);
        *cmdbuf++ = destbuf.va & UINT32_MAX;
        *cmdbuf++ = destbuf.va >> 32;
        *cmdbuf++ = 0xdeadbeaf + i;
        *cmdbuf++ = 0xdeadbeaf;
        *cmdbuf++ = 0xdeadbeaf;
        *cmdbuf++ = 0xdeadbeaf;
        *cmdbuf++ = 0xdeadbeaf;

        __sync_synchronize();
        sleep(1);

        fprintf(stderr, "%.8x\n", (uint32_t)(cmdbuf - cmdbuf_base));
        *(volatile std::uint64_t*)wptr.ptr = (cmdbuf - cmdbuf_base);
        __sync_synchronize();
        *(volatile std::uint64_t*)doorbell.ptr = (cmdbuf - cmdbuf_base);
        __sync_synchronize();

        sleep(1);

        printf("%.8x %.8x %.8x\n", *(uint32_t*)destbuf.ptr, *(uint32_t*)rptr.ptr, *(uint32_t*)wptr.ptr);
    }
    
    dev.destroy_queue(queue);

    return 0;
}
